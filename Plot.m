clear all 
np=200;
B1=linspace(0,-20,np);
B2=linspace(0,20,np);

d1_1=readmatrix('d1_1.txt');
d2_1=readmatrix('d2_1.txt');
d1_2=readmatrix('d1_2.txt');
d2_2=readmatrix('d2_2.txt');

figure(1)
    plot(B1,d1_1,'LineWidth',1)
    hold on 
    plot(B2,d1_2,'LineWidth',1)
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    %ylim([-0.015 0.015])
    %set ( gca, 'xdir', 'reverse' )
    
figure(2)
    plot(B1,d2_1,'LineWidth',1)
    hold on 
    plot(B2,d2_2,'LineWidth',1)
    %ylim([-20e-3 20e-3])
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    