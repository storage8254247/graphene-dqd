clear all
%Vmax=-200e-4;Vmin=200e-4;
res=100;
Vol=1e-4;
dV=1e-4;
%np=res;  
%Vol=linspace(Vmin,Vmax,np);
%dV=Vol(2)-Vol(1); 
hbar=1.055e-34;q=1.609e-19;I0=q^2/hbar;eta=0.5;
%Gate Voltage Range
muf=linspace(50e-3,60e-3,res);nf=size(muf,2);
%muf=0.00;nf=1; 
gamma1=5e-6;
gamma2=5e-6;
vec=[zeros(1,255) 1]';kBT=9e-5;beta=0;
Vg=0;
%delv=[0.1 0.09];

c1=zeros(256);c2=zeros(256);c3=zeros(256);c4=zeros(256);
c5=zeros(256);c6=zeros(256);c7=zeros(256);c8=zeros(256);
es=0e-3;del=0e-3;zp=0e-8; 
ep1=es;
ep2=es+del; 
U11=0e-3;U22=4e-3;U12=0e-3;U21=2e-3;
tt=-0.5e-3;
ts=-0.5e-3;
delta=10e-3; 

mu_B = 5.79e-5;
g_s = 2;
g_v = 15; 
np=res;B_ll=linspace(0,20,np);B_per=0;B=B_ll;
%np=res;B_per=linspace(0,20,np);B_ll=0;B=B_per;
hs = 0.5*g_s*mu_B*B_ll;
hv = 0.5*g_v*mu_B*B_per;

%-tt/del;  % What does this mean?
U=[U11 U12;U21 U22];
%annihilation operators c1,c2,c3,c4

d0=[];
d1=[];
d2=[];
d3=[];
d4=[];
d5=[];
d6=[];
d7=[];
d8=[];

for k1=1:2
for k2=1:2
for k3=1:2
for k4=1:2
for k5=1:2
for k6=1:2
for k7=1:2
        kp = [1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [0 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c8(k+1,kp+1)=(-1)^(k7-1+k6-1+k5-1+k4-1+k3-1+k2-1+k1-1);   
        kp = [k7-1 1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 0 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c7(k+1,kp+1)=(-1)^(k6-1+k5-1+k4-1+k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 0 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c6(k+1,kp+1)=(-1)^(k5-1+k4-1+k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 k5-1 1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 0 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c5(k+1,kp+1)=(-1)^(k4-1+k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 k5-1 k4-1 1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 0 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c4(k+1,kp+1)=(-1)^(k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 k5-1 k4-1 k3-1 1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 k3-1 0 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c3(k+1,kp+1)=(-1)^(k2-1+k1-1);
        kp = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 0 k1-1]*[128;64;32;16;8;4;2;1];
        c2(k+1,kp+1)=(-1)^(k1-1);
        kp = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1 1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1 0]*[128;64;32;16;8;4;2;1];
        c1(k+1,kp+1)=1;            
end
end
end
end
end
end
end

for vc=1:np
%i0,i1,i2,i3,i4 contain a list of columnends corresponding to
%states with 0,1,2,3 and 4 electrons respectively
i8=[];i7=[];i6=[];i5=[];i4=[];i3=[];i2=[];i1=[];i0=[];
%diagonal terms of Hamiltonian matrix, H
% and number operator, N 
H=zeros(256);N=zeros(256); 
for k7=1:2
for k8=1:2
for k6=1:2
for k5=1:2
for k4=1:2
for k3=1:2
for k2=1:2
for k1=1:2
        k = [k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        n1=k1-1+k2-1+k3-1+k4-1;
        n2=k5-1+k6-1+k7-1+k8-1;
        n=n1+n2;
        H(k+1,k+1)=ep1*n1+ep2*n2 + (U11/2)*(n1^2-n1+n2^2-n2) + U12*n1*n2; %+...
            %(delta/2)*[k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[1;1;-1;-1;1;1;-1;-1] +...
            %hs(vc)*[k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[1;-1;-1;1;1;-1;-1;1] +... 
            %hv(vc)*[k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[1;-1;1;-1;1;-1;1;-1]; 
        N(k+1,k+1)=n; 
        if n==0
                i0=[k+1;i0];
        end
        if n==1
                i1=[k+1;i1];
        end
        if n==2
                i2=[k+1;i2];
        end
        if n==3
                i3=[k+1;i3];
        end
        if n==4
                i4=[k+1;i4];
        end
        if n==5
                i5=[k+1;i5];
        end
        if n==6
                i6=[k+1;i6];
        end
        if n==7
                i7=[k+1;i7];
        end
        if n==8
                i8=[k+1;i8];
        end
end
end
end
end
end
end
end
end 
%off-diagonal terms of [H] due to overlap
%{
for k1=1:2
for k2=1:2
for k3=1:2
for k4=1:2
for k5=1:2
for k6=1:2
    % 4 <--> 8
    kp = [0 k6-1 k5-1 k4-1 1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    k = [1 k6-1 k5-1 k4-1 0 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    H(k+1,kp+1)=tt*((-1)^(k4-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
    % 3 <--> 7
    kp = [k6-1 0 k5-1 k4-1 k3-1 1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    k = [k6-1 1 k5-1 k4-1 k3-1 0 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    H(k+1,kp+1)=tt*((-1)^(k3-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
    % 2 <--> 6
    kp = [k6-1 k5-1 0 k4-1 k3-1 k2-1 1 k1-1]*[128;64;32;16;8;4;2;1];
    k = [k6-1 k5-1 1 k4-1 k3-1 k2-1 0 k1-1]*[128;64;32;16;8;4;2;1];
    H(k+1,kp+1)=tt*((-1)^(k2-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
    % 1 <--> 5
    kp = [k6-1 k5-1 k4-1 0 k3-1 k2-1 k1-1 1]*[128;64;32;16;8;4;2;1];
    k = [k6-1 k5-1 k4-1 1 k3-1 k2-1 k1-1 0]*[128;64;32;16;8;4;2;1]; 
    H(k+1,kp+1)=tt*((-1)^(k1-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
end
end
end
end
end
end
%}
H = H + tt*(c5'*c1+c1'*c5+c6'*c2+c2'*c6+c7'*c3+c3'*c7+c8'*c4+c4'*c8); 
H = H + (delta/2)*(-c1'*c1-c2'*c2+c3'*c3+c4'*c4-c5'*c5-c6'*c6+c7'*c7+c8'*c8);
H = H + hs(vc)*(c1'*c1-c2'*c2-c3'*c3+c4'*c4+c5'*c5-c6'*c6-c7'*c7+c8'*c8);
H = H + hv*(-c1'*c1+c2'*c2-c3'*c3+c4'*c4-c5'*c5+c6'*c6-c7'*c7+c8'*c8);
H = H + 2*ts*(c8'*c1+c1'*c8+c7'*c2+c2'*c7+c6'*c3+c3'*c6+c5'*c4+c4'*c5);

%Separating the parts of the Hamiltonian
%with different numbers of electrons
H0=H([i0],[i0]);H1=H([i1],[i1]);H2=H([i2],[i2]);
H3=H([i3],[i3]);H4=H([i4],[i4]);H5=H([i5],[i5]);
H6=H([i6],[i6]);H7=H([i7],[i7]);H8=H([i8],[i8]);
%Finding eigenvalues and eigenvectors
% for Hamiltonians with 0,1,2,3 and 4 electrons
[V0,D0]=eig(H0);D0=real(sum(D0))';[a0,b0]=sort(D0);
[V1,D1]=eig(H1);D1=real(sum(D1))';[a1,b1]=sort(D1); 
[V2,D2]=eig(H2);D2=real(sum(D2))';[a2,b2]=sort(D2);
[V3,D3]=eig(H3);D3=real(sum(D3))';[a3,b3]=sort(D3);
[V4,D4]=eig(H4);D4=real(sum(D4))';[a4,b4]=sort(D4);
[V5,D5]=eig(H5);D5=real(sum(D5))';[a5,b5]=sort(D5); 
[V6,D6]=eig(H6);D6=real(sum(D6))';[a6,b6]=sort(D6);
[V7,D7]=eig(H7);D7=real(sum(D7))';[a7,b7]=sort(D7);
[V8,D8]=eig(H8);D8=real(sum(D8))';[a8,b8]=sort(D8);

d0(vc,:)=D0;
d1(vc,:)=D1;
d2(vc,:)=D2;
d3(vc,:)=D3;
d4(vc,:)=D4;
d5(vc,:)=D5;
d6(vc,:)=D6;
d7(vc,:)=D7;
d8(vc,:)=D8;

end

writematrix(d1);
writematrix(d2);
writematrix(d3);
writematrix(d4);

%figure(1)
    %plot(B,d0,'LineWidth',1)
figure(1)
    plot(B,d1,'LineWidth',1)
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    %ylimit([1e-3 2e-3])
figure(2)
    plot(B,d2,'LineWidth',1)
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    ylim([-30e-3 30e-3])
figure(3)
    plot(B,d3,'LineWidth',1)
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    ylim([-35e-3 35e-3])
figure(4)
    plot(B,d4,'LineWidth',1)
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    ylim([-40e-3 40e-3])
%figure(6)
%    plot(B,d5,'LineWidth',1)
%figure(7)
%    plot(B,d6,'LineWidth',1)
%figure(8)
%    plot(B,d7,'LineWidth',1)
%figure(9)
%    plot(B,d8,'LineWidth',1)
    