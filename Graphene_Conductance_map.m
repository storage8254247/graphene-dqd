clear all
%Vmax=-200e-4;Vmin=200e-4;
res=500;
Vol=1e-4;
dV=2e-4;
%np=res;  
%Vol=linspace(Vmin,Vmax,np);
%dV=Vol(2)-Vol(1); 
hbar=1.055e-34;q=1.609e-19;I0=q^2/hbar;eta=0.5;

%Gate Voltage Range
nf=res;
muf1=linspace(-15e-3,15e-3,res);
muf2=linspace(-30e-3,30e-3,res);
muf3=linspace(-30e-3,30e-3,res);
muf4=linspace(-30e-3,30e-3,res);

%muf=0.00;nf=1; 
gamma1=5e-6;
gamma2=5e-6;
vec=[zeros(1,255) 1]';kBT=9e-5;beta=0;
Vg=0;

c1=zeros(256);c2=zeros(256);c3=zeros(256);c4=zeros(256);
c5=zeros(256);c6=zeros(256);c7=zeros(256);c8=zeros(256);
es=0e-3;del=0e-3;zp=0e-8; 
ep1=es;
ep2=es+del; 
U11=0e-3;U12=0e-3;
tt=-0.5e-3;
ts=-0.5e-3;
delta=10e-3; 

mu_B = 5.79e-5;
g_s = 2;
g_v = 15; 
%np=res;B_ll=linspace(0,20,np);B_per=0;B=B_ll;
np=res;B_per=linspace(0,20,np);B_ll=0;B=B_per;
%np=1;B=0;
hs = 0.5*g_s*mu_B*B_ll;
hv = 0.5*g_v*mu_B*B_per;
I=zeros(np,nf);

%annihilation operators c1,c2,c3,c4
for k1=1:2
for k2=1:2
for k3=1:2
for k4=1:2
for k5=1:2
for k6=1:2
for k7=1:2
        kp = [1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [0 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c8(k+1,kp+1)=(-1)^(k7-1+k6-1+k5-1+k4-1+k3-1+k2-1+k1-1);   
        kp = [k7-1 1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 0 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c7(k+1,kp+1)=(-1)^(k6-1+k5-1+k4-1+k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 0 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c6(k+1,kp+1)=(-1)^(k5-1+k4-1+k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 k5-1 1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 0 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c5(k+1,kp+1)=(-1)^(k4-1+k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 k5-1 k4-1 1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 0 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c4(k+1,kp+1)=(-1)^(k3-1+k2-1+k1-1); 
        kp = [k7-1 k6-1 k5-1 k4-1 k3-1 1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 k3-1 0 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        c3(k+1,kp+1)=(-1)^(k2-1+k1-1);
        kp = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 1 k1-1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 0 k1-1]*[128;64;32;16;8;4;2;1];
        c2(k+1,kp+1)=(-1)^(k1-1);
        kp = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1 1]*[128;64;32;16;8;4;2;1];
        k = [k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1 0]*[128;64;32;16;8;4;2;1];
        c1(k+1,kp+1)=1;            
end
end
end
end
end
end
end

for vc=1:np
    fprintf("Processing %d of %d...\n",vc,np);
%i0,i1,i2,i3,i4 contain a list of columnends corresponding to
%states with 0,1,2,3 and 4 electrons respectively
i8=[];i7=[];i6=[];i5=[];i4=[];i3=[];i2=[];i1=[];i0=[];
%diagonal terms of Hamiltonian matrix, H
% and number operator, N 
H=zeros(256);N=zeros(256); 
for k7=1:2
for k8=1:2
for k6=1:2
for k5=1:2
for k4=1:2
for k3=1:2
for k2=1:2
for k1=1:2
        k = [k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
        n1=k1-1+k2-1+k3-1+k4-1;
        n2=k5-1+k6-1+k7-1+k8-1;
        n=n1+n2;
        H(k+1,k+1)=ep1*n1+ep2*n2 + (U11/2)*(n1^2-n1+n2^2-n2) + U12*n1*n2; %+...
            %(delta/2)*[k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[1;1;-1;-1;1;1;-1;-1] +...
            %hs(vc)*[k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[1;-1;-1;1;1;-1;-1;1] +... 
            %hv(vc)*[k8-1 k7-1 k6-1 k5-1 k4-1 k3-1 k2-1 k1-1]*[1;-1;1;-1;1;-1;1;-1]; 
        N(k+1,k+1)=n; 
        if n==0
                i0=[k+1;i0];
        end
        if n==1
                i1=[k+1;i1];
        end
        if n==2
                i2=[k+1;i2];
        end
        if n==3
                i3=[k+1;i3];
        end
        if n==4
                i4=[k+1;i4];
        end
        if n==5
                i5=[k+1;i5];
        end
        if n==6
                i6=[k+1;i6];
        end
        if n==7
                i7=[k+1;i7];
        end
        if n==8
                i8=[k+1;i8];
        end
end
end
end
end
end
end
end
end 
%off-diagonal terms of [H] due to overlap
%{
for k1=1:2
for k2=1:2
for k3=1:2
for k4=1:2
for k5=1:2
for k6=1:2
    % 4 <--> 8
    kp = [0 k6-1 k5-1 k4-1 1 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    k = [1 k6-1 k5-1 k4-1 0 k3-1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    H(k+1,kp+1)=tt*((-1)^(k4-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
    % 3 <--> 7
    kp = [k6-1 0 k5-1 k4-1 k3-1 1 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    k = [k6-1 1 k5-1 k4-1 k3-1 0 k2-1 k1-1]*[128;64;32;16;8;4;2;1];
    H(k+1,kp+1)=tt*((-1)^(k3-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
    % 2 <--> 6
    kp = [k6-1 k5-1 0 k4-1 k3-1 k2-1 1 k1-1]*[128;64;32;16;8;4;2;1];
    k = [k6-1 k5-1 1 k4-1 k3-1 k2-1 0 k1-1]*[128;64;32;16;8;4;2;1];
    H(k+1,kp+1)=tt*((-1)^(k2-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
    % 1 <--> 5
    kp = [k6-1 k5-1 k4-1 0 k3-1 k2-1 k1-1 1]*[128;64;32;16;8;4;2;1];
    k = [k6-1 k5-1 k4-1 1 k3-1 k2-1 k1-1 0]*[128;64;32;16;8;4;2;1]; 
    H(k+1,kp+1)=tt*((-1)^(k1-1)); 
    H(kp+1,k+1)=H(k+1,kp+1);
end
end
end
end
end
end
%}
H = H + tt*(c5'*c1+c1'*c5+c6'*c2+c2'*c6+c7'*c3+c3'*c7+c8'*c4+c4'*c8); 
H = H + (delta/2)*(-c1'*c1-c2'*c2+c3'*c3+c4'*c4-c5'*c5-c6'*c6+c7'*c7+c8'*c8);
H = H + hs*(c1'*c1-c2'*c2-c3'*c3+c4'*c4+c5'*c5-c6'*c6-c7'*c7+c8'*c8);
H = H + hv(vc)*(-c1'*c1+c2'*c2-c3'*c3+c4'*c4-c5'*c5+c6'*c6-c7'*c7+c8'*c8);
H = H + 2*ts*(c8'*c1+c1'*c8+c7'*c2+c2'*c7+c6'*c3+c3'*c6+c5'*c4+c4'*c5);
%Separating the parts of the Hamiltonian
%with different numbers of electrons
H0=H([i0],[i0]);H1=H([i1],[i1]);H2=H([i2],[i2]);
H3=H([i3],[i3]);H4=H([i4],[i4]);H5=H([i5],[i5]);
H6=H([i6],[i6]);H7=H([i7],[i7]);H8=H([i8],[i8]);
%Finding eigenvalues and eigenvectors
% for Hamiltonians with 0,1,2,3 and 4 electrons
[V0,D0]=eig(H0);D0=real(sum(D0))';[a0,b0]=sort(D0);
[V1,D1]=eig(H1);D1=real(sum(D1))';[a1,b1]=sort(D1); 
[V2,D2]=eig(H2);D2=real(sum(D2))';[a2,b2]=sort(D2);
[V3,D3]=eig(H3);D3=real(sum(D3))';[a3,b3]=sort(D3);
[V4,D4]=eig(H4);D4=real(sum(D4))';[a4,b4]=sort(D4);
[V5,D5]=eig(H5);D5=real(sum(D5))';[a5,b5]=sort(D5); 
[V6,D6]=eig(H6);D6=real(sum(D6))';[a6,b6]=sort(D6);
[V7,D7]=eig(H7);D7=real(sum(D7))';[a7,b7]=sort(D7);
[V8,D8]=eig(H8);D8=real(sum(D8))';[a8,b8]=sort(D8);
%need annihilation operator between  g  and h  subspaces  
% What does g and h mean ? 
 c11=c1([i0],[i1]);c12=c2([i0],[i1]);
 c13=c3([i0],[i1]);c14=c4([i0],[i1]);
 c15=c5([i0],[i1]);c16=c6([i0],[i1]);
 c17=c7([i0],[i1]);c18=c8([i0],[i1]);
 c21=c1([i1],[i2]);c22=c2([i1],[i2]);
 c23=c3([i1],[i2]);c24=c4([i1],[i2]);
 c25=c5([i1],[i2]);c26=c6([i1],[i2]);
 c27=c7([i1],[i2]);c28=c8([i1],[i2]);
 c31=c1([i2],[i3]);c32=c2([i2],[i3]);
 c33=c3([i2],[i3]);c34=c4([i2],[i3]);
 c35=c5([i2],[i3]);c36=c6([i2],[i3]);
 c37=c7([i2],[i3]);c38=c8([i2],[i3]);
 c41=c1([i3],[i4]);c42=c2([i3],[i4]);
 c43=c3([i3],[i4]);c44=c4([i3],[i4]);
 c45=c5([i3],[i4]);c46=c6([i3],[i4]);
 c47=c7([i3],[i4]);c48=c8([i3],[i4]);
 c51=c1([i4],[i5]);c52=c2([i4],[i5]);
 c53=c3([i4],[i5]);c54=c4([i4],[i5]);
 c55=c5([i4],[i5]);c56=c6([i4],[i5]);
 c57=c7([i4],[i5]);c58=c8([i4],[i5]);
 c61=c1([i5],[i6]);c62=c2([i5],[i6]);
 c63=c3([i5],[i6]);c64=c4([i5],[i6]);
 c65=c5([i5],[i6]);c66=c6([i5],[i6]);
 c67=c7([i5],[i6]);c68=c8([i5],[i6]);
 c71=c1([i6],[i7]);c72=c2([i6],[i7]);
 c73=c3([i6],[i7]);c74=c4([i6],[i7]);
 c75=c5([i6],[i7]);c76=c6([i6],[i7]);
 c77=c7([i6],[i7]);c78=c8([i6],[i7]);
 c81=c1([i7],[i8]);c82=c2([i7],[i8]);
 c83=c3([i7],[i8]);c84=c4([i7],[i8]);
 c85=c5([i7],[i8]);c86=c6([i7],[i8]);
 c87=c7([i7],[i8]);c88=c8([i7],[i8]);
%Evaluate gamma1 and gamma2 using Fermi’s Golden Rule
s0=size([i0],1);s1=size([i1],1);s2=size([i2],1);
s3=size([i3],1);s4=size([i4],1);s5=size([i5],1); 
s6=size([i6],1);s7=size([i7],1);s8=size([i8],1);
for ct1=1:s0
    for ct2=1:s1
        eh1(ct1,ct2)=a1(ct2)-a0(ct1);
        ev0(:,ct1)=V0(:,b0(ct1));
        ev1(:,ct2)=V1(:,b1(ct2)); 
    end
end
A11=[c11*ev1]'*ev0;
B11=[c12*ev1]'*ev0;
C11=[c13*ev1]'*ev0; 
D11=[c14*ev1]'*ev0; 
gam11=gamma1*((A11.*A11)+(B11.*B11)+(C11.*C11)+(D11.*D11)); 
A21=[c15*ev1]'*ev0;
B21=[c16*ev1]'*ev0;
C21=[c17*ev1]'*ev0; 
D21=[c18*ev1]'*ev0; 
gam21=gamma2*((A21.*A21)+(B21.*B21)+(C21.*C21)+(D21.*D21));  
for ct1=1:s1
    for ct2=1:s2
        eh2(ct1,ct2)=a2(ct2)-a1(ct1);
        ev2(:,ct2)=V2(:,b2(ct2));
    end
end
A12=[c21*ev2]'*ev1;
B12=[c22*ev2]'*ev1;
C12=[c23*ev2]'*ev1;
D12=[c24*ev2]'*ev1;
gam12=gamma1*((A12.*A12)+(B12.*B12)+(C12.*C12)+(D12.*D12));
A22=[c25*ev2]'*ev1;                   
B22=[c26*ev2]'*ev1;
C22=[c27*ev2]'*ev1;
D22=[c28*ev2]'*ev1;
gam22=gamma2*((A22.*A22)+(B22.*B22)+(C22.*C22)+(D22.*D22)); 
ev3=zeros(s3,s3);
for ct1=1:s2
    for ct2=1:s3
        eh3(ct1,ct2)=a3(ct2)-a2(ct1);
        ev3(:,ct2)=V3(:,b3(ct2));
    end
end
A13=[c31*ev3]'*ev2;
B13=[c32*ev3]'*ev2;
C13=[c33*ev3]'*ev2;
D13=[c34*ev3]'*ev2;
gam13=gamma1*((A13.*A13)+(B13.*B13)+(C13.*C13)+(D13.*D13));
A23=[c35*ev3]'*ev2;
B23=[c36*ev3]'*ev2;
C23=[c37*ev3]'*ev2;
D23=[c38*ev3]'*ev2;
gam23=gamma2*((A23.*A23)+(B23.*B23)+(C23.*C23)+(D23.*D23)); 
for ct1=1:s3
    for ct2=1:s4
        eh4(ct1,ct2)=a4(ct2)-a3(ct1);
        ev4(:,ct2)=V4(:,b4(ct2));
    end
end
A14=[c41*ev4]'*ev3;
B14=[c42*ev4]'*ev3;
C14=[c43*ev4]'*ev3;
D14=[c44*ev4]'*ev3;
gam14=gamma1*((A14.*A14)+(B14.*B14)+(C14.*C14)+(D14.*D14));
A24=[c45*ev4]'*ev3;
B24=[c46*ev4]'*ev3;
C24=[c47*ev4]'*ev3;
D24=[c48*ev4]'*ev3;
gam24=gamma2*((A24.*A24)+(B24.*B24)+(C24.*C24)+(D24.*D24));
for ct1=1:s4
    for ct2=1:s5
        eh5(ct1,ct2)=a5(ct2)-a4(ct1);
        ev5(:,ct2)=V5(:,b5(ct2));
    end
end
A15=[c51*ev5]'*ev4;
B15=[c52*ev5]'*ev4;
C15=[c53*ev5]'*ev4;
D15=[c54*ev5]'*ev4;
gam15=gamma1*((A15.*A15)+(B15.*B15)+(C15.*C15)+(D15.*D15));
A25=[c55*ev5]'*ev4;
B25=[c56*ev5]'*ev4;
C25=[c57*ev5]'*ev4;
D25=[c58*ev5]'*ev4;
gam25=gamma2*((A25.*A25)+(B25.*B25)+(C25.*C25)+(D25.*D25)); 
for ct1=1:s5
    for ct2=1:s6
        eh6(ct1,ct2)=a6(ct2)-a5(ct1);
        ev6(:,ct2)=V6(:,b6(ct2)); 
    end
end
A16=[c61*ev6]'*ev5;
B16=[c62*ev6]'*ev5;
C16=[c63*ev6]'*ev5;
D16=[c64*ev6]'*ev5;
gam16=gamma1*((A16.*A16)+(B16.*B16)+(C16.*C16)+(D16.*D16));
A26=[c65*ev6]'*ev5;
B26=[c66*ev6]'*ev5; 
C26=[c67*ev6]'*ev5; 
D26=[c68*ev6]'*ev5; 
gam26=gamma2*((A26.*A26)+(B26.*B26)+(C26.*C26)+(D26.*D26)); 
for ct1=1:s6
    for ct2=1:s7
        eh7(ct1,ct2)=a7(ct2)-a6(ct1);
        ev7(:,ct2)=V7(:,b7(ct2)); 
    end
end
A17=[c71*ev7]'*ev6;
B17=[c72*ev7]'*ev6;
C17=[c73*ev7]'*ev6;
D17=[c74*ev7]'*ev6;
gam17=gamma1*((A17.*A17)+(B17.*B17)+(C17.*C17)+(D17.*D17));
A27=[c75*ev7]'*ev6;
B27=[c76*ev7]'*ev6; 
C27=[c77*ev7]'*ev6; 
D27=[c78*ev7]'*ev6; 
gam27=gamma2*((A27.*A27)+(B27.*B27)+(C27.*C27)+(D27.*D27)); 
for ct1=1:s7
    for ct2=1:s8
        eh8(ct1,ct2)=a8(ct2)-a7(ct1);
        ev8(:,ct2)=V8(:,b8(ct2)); 
    end
end
A18=[c81*ev8]'*ev7;
B18=[c82*ev8]'*ev7;
C18=[c83*ev8]'*ev7;
D18=[c84*ev8]'*ev7;
gam18=gamma1*((A18.*A18)+(B18.*B18)+(C18.*C18)+(D18.*D18));
A28=[c85*ev8]'*ev7;
B28=[c86*ev8]'*ev7; 
C28=[c87*ev8]'*ev7; 
D28=[c88*ev8]'*ev7; 
gam28=gamma2*((A28.*A28)+(B28.*B28)+(C28.*C28)+(D28.*D28));  

for vf=1:nf 
    muL=muf1(vf)+0.5*Vol;muR=muf1(vf)-0.5*Vol;
      RL=zeros(256);
      R=zeros(256);
      f11=1./(1+exp((eh1-muL)/kBT)); 
      f21=1./(1+exp((eh1-muR)/kBT));
      f12=1./(1+exp((eh2-muL)/kBT));
      f22=1./(1+exp((eh2-muR)/kBT));
      f13=1./(1+exp((eh3-muL)/kBT));
      f23=1./(1+exp((eh3-muR)/kBT));
      f14=1./(1+exp((eh4-muL)/kBT));
      f24=1./(1+exp((eh4-muR)/kBT));
      f15=1./(1+exp((eh5-muL)/kBT));
      f25=1./(1+exp((eh5-muR)/kBT));
      f16=1./(1+exp((eh6-muL)/kBT));
      f26=1./(1+exp((eh6-muR)/kBT));
      f17=1./(1+exp((eh7-muL)/kBT));
      f27=1./(1+exp((eh7-muR)/kBT));
      f18=1./(1+exp((eh8-muL)/kBT));
      f28=1./(1+exp((eh8-muR)/kBT)); 
      sti=1;stj=2;eni=s0;enj=1+s1;  %Why start column number from 2 instead of 1? 
      R(sti:eni,stj:enj)=gam11'.*(1-f11)+gam21'.*(1-f21);  % Why transpose gamma?
      RL(sti:eni,stj:enj)=-gam11'.*(1-f11); 
      R(stj:enj,sti:eni)=(gam11'.*f11+gam21'.*f21)';   % Why transpose the entire rate matrix?
      RL(stj:enj,sti:eni)=(gam11'.*f11)';
      sti=eni+1;stj=enj+1;eni=sti+s1-1;enj=stj+s2-1;
      R(sti:eni,stj:enj)=gam12'.*(1-f12)+gam22'.*(1-f22);
      RL(sti:eni,stj:enj)=-gam12'.*(1-f12);
      R(stj:enj,sti:eni)=(gam12'.*f12+gam22'.*f22)';
      RL(stj:enj,sti:eni)=(gam12'.*f12)';
      sti=eni+1;stj=enj+1;eni=sti+s2-1;enj=stj+s3-1;
      R(sti:eni,stj:enj)=gam13'.*(1-f13)+gam23'.*(1-f23);
      RL(sti:eni,stj:enj)=-gam13'.*(1-f13);
      R(stj:enj,sti:eni)=(gam13'.*f13+gam23'.*f23)';
      RL(stj:enj,sti:eni)=(gam13'.*f13)';
      sti=eni+1;stj=enj+1;eni=sti+s3-1;enj=stj+s4-1;
      R(sti:eni,stj:enj)=gam14'.*(1-f14)+gam24'.*(1-f24);
      RL(sti:eni,stj:enj)=-gam14'.*(1-f14);
      R(stj:enj,sti:eni)=(gam14'.*f14+gam24'.*f24)';
      RL(stj:enj,sti:eni)=(gam14'.*f14)';
      sti=eni+1;stj=enj+1;eni=sti+s4-1;enj=stj+s5-1;
      R(sti:eni,stj:enj)=gam15'.*(1-f15)+gam25'.*(1-f25);
      RL(sti:eni,stj:enj)=-gam15'.*(1-f15);
      R(stj:enj,sti:eni)=(gam15'.*f15+gam25'.*f25)';
      RL(stj:enj,sti:eni)=(gam15'.*f15)';
      sti=eni+1;stj=enj+1;eni=sti+s5-1;enj=stj+s6-1;
      R(sti:eni,stj:enj)=gam16'.*(1-f16)+gam26'.*(1-f26);
      RL(sti:eni,stj:enj)=-gam16'.*(1-f16);
      R(stj:enj,sti:eni)=(gam16'.*f16+gam26'.*f26)';
      RL(stj:enj,sti:eni)=(gam16'.*f16)';
      sti=eni+1;stj=enj+1;eni=sti+s6-1;enj=stj+s7-1;
      R(sti:eni,stj:enj)=gam17'.*(1-f17)+gam27'.*(1-f27);
      RL(sti:eni,stj:enj)=-gam17'.*(1-f17);
      R(stj:enj,sti:eni)=(gam17'.*f17+gam27'.*f27)';
      RL(stj:enj,sti:eni)=(gam17'.*f17)';
      sti=eni+1;stj=enj+1;eni=sti+s7-1;enj=stj+s8-1;
      R(sti:eni,stj:enj)=gam18'.*(1-f18)+gam28'.*(1-f28);
      RL(sti:eni,stj:enj)=-gam18'.*(1-f18);
      R(stj:enj,sti:eni)=(gam18'.*f18+gam28'.*f28)';
      RL(stj:enj,sti:eni)=(gam18'.*f18)'; 
      for de=1:255
           R(de,de)=-sum(R(:,de));   % Why do this? 
      end
      R(256,:)=1;
      Pr=R\vec;
      I(vc,vf)=sum(RL*Pr);
end
end

I=I0*I;
cond=(I/Vol);
cond1=zeros(np,nf);
cond2=zeros(np,nf);
cond3=zeros(np,nf);
cond4=zeros(np,nf);
cond5=zeros(np,nf);

for i=1:np 
    [pks,locs] = findpeaks(cond(i,:),'MinPeakHeight',1e-8);
    cond1(i,locs)=1;
    u=muf1(locs);
    %{
    if length(locs)>=2
    v2=nchoosek(u,2);
    w2=sum(v2,2);
    z2=interp1(muf2,muf2,w2,'nearest');
    for j=1:length(z2)
        locs2(j)=find(muf2==z2(j));
    end 
    cond2(i,locs2)=1;
    end
    
    if length(locs)>=3
    v3=nchoosek(u,3);
    w3=sum(v3,2);
    z3=interp1(muf3,muf3,w3,'nearest');
    for j=1:length(z3)
        locs3(j)=find(muf3==z3(j));
    end 
    cond3(i,locs3)=1;
    end

    if length(locs)>=4
    v4=nchoosek(u,4);
    w4=sum(v4,2);
    z4=interp1(muf4,muf4,w4,'nearest');
    for j=1:length(z4)
        locs4(j)=find(muf4==z4(j));
    end 
    cond4(i,locs4)=1;
    end   
    %}
end 
%{
figure(4)
    [y,x] = meshgrid(muf4,B);
    pcolor(x,y,cond4); 
    colorbar;
    shading flat
figure(3)
    [y,x] = meshgrid(muf3,B);
    pcolor(x,y,cond3); 
    colorbar;
    shading flat
figure(2)
    [y,x] = meshgrid(muf2,B);
    pcolor(x,y,cond2); 
    colorbar;
    shading flat
%}
figure(1)
    [y,x] = meshgrid(muf1,B);
    pcolor(x,y,cond1); 
    colorbar;
    shading flat     