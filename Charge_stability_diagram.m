clear all
min=0;
res=200;
np=1;ng=1;nf=1;
%Vol=linspace(Vmin,Vmax,np);dV=Vol(2)-Vol(1);
Vol=9e-4; 
hbar=6.582e-16;q=1.609e-19;I0=q^2/hbar;eta=0.5;
%Gate Voltage Range
%muf=linspace(0.0028,0.0080,100);nf=size(muf,2);
muf=0;
gamma1=5e-6;
gamma2=5e-6;
kBT=9e-5;beta=0;
Vg=0;
%delv=[0.1 0.09];
c1=zeros(16);c2=zeros(16);c3=zeros(16);c4=zeros(16);
es=0.0;del=0e-3;zp=0e-8;

U11=3e-3;U12=1e-3;
tt=-0.1e-3;tau1=0.1;
%-tt/del;

%annihilation operators c1,c2,c3,c4
for k1=1:2
for k2=1:2
for k3=1:2
         kp=[1 k3-1 k2-1 k1-1]*[8;4;2;1];
         k=[0 k3-1 k2-1 k1-1]*[8;4;2;1];
         c4(k+1,kp+1)=(-1)^(k1-1+k2-1+k3-1);
         kp=[k3-1 1 k2-1 k1-1]*[8;4;2;1];
         k=[k3-1 0 k2-1 k1-1]*[8;4;2;1];
         c3(k+1,kp+1)=(-1)^(k1-1+k2-1);
         kp=[k3-1 k2-1 1 k1-1]*[8;4;2;1];
         k=[k3-1 k2-1 0 k1-1]*[8;4;2;1];
         c2(k+1,kp+1)=(-1)^(k1-1);
         kp=[k3-1 k2-1 k1-1 1]*[8;4;2;1];
         k=[k3-1 k2-1 k1-1 0]*[8;4;2;1];
         c1(k+1,kp+1)=1;
end
end
end
%d1=c1';d2=c2';d3=c3';d4=c4';%creation operators
%i0,i1,i2,i3,i4 contain a list of columnends corresponding to
%states with 0,1,2,3 and 4 electrons respectively
ng1=res;
ng2=res;
Vmax=3e-3;Vmin=-10e-3;
Vg1=linspace(Vmin,Vmax,ng1);
Vg2=linspace(Vmin,Vmax,ng2); 

I=zeros(ng1,ng2); 
for g1=1:ng1
fprintf("Processing %d of %d...\n",g1,ng1);
for g2=1:ng2 

    H=zeros(16);N=zeros(16);
    vec=[zeros(1,15) 1]'; 
    %ep=[(es+del+Vg2(g2)-zp),(es+del+Vg2(g2)+zp),(es+Vg1(g1)-zp),(es+Vg1(g1)+zp)]; 
    ep1=es+Vg1(g1);
    ep2=es+del+Vg2(g2); 

    i4=[];i3=[];i2=[];i1=[];i0=[]; 
%diagonal terms of Hamiltonian matrix, H
% and number operator, N
for k1=1:2
for k2=1:2
for k3=1:2
for k4=1:2
        k=[k4-1 k3-1 k2-1 k1-1]*[8;4;2;1];
        %Ud=diag(diag(U));
        %nv=[k1+k2-2;k3+k4-2];
        n1=k1-1+k2-1;
        n2=k4-1+k3-1;
        n=n1+n2;
        H(k+1,k+1)=ep1*n1+ep2*n2 + (U11/2)*(n1^2-n1+n2^2-n2) + U12*n1*n2;
        N(k+1,k+1)=n; 
        if n==0
                i0=[k+1;i0]; 
        end
        if n==1
                i1=[k+1;i1];
        end
        if n==2
                i2=[k+1;i2];
        end
        if n==3
                i3=[k+1;i3];
        end
        if n==4
                i4=[k+1,i4];
        end
end
end
end
end
%off-diagonal terms of [H] due to overlap
for k1=1:2
for k2=1:2
        % 2 <--> 4
kp=[0 k2-1 1 k1-1]*[8;4;2;1];
k=[1 k2-1 0 k1-1]*[8;4;2;1];
H(k+1,kp+1)=tt*((-1)^(k2-1));
H(kp+1,k+1)=H(k+1,kp+1);
        % 1 <--> 3
kp=[k2-1 0 k1-1 1]*[8;4;2;1];
k=[k2-1 1 k1-1 0]*[8;4;2;1];
H(k+1,kp+1)=tt*((-1)^(k1-1));
H(kp+1,k+1)=H(k+1,kp+1);
end
end
%Separating the parts of the Hamiltonian
%with different numbers of electrons
H0=H([i0],[i0]);H1=H([i1],[i1]);H2=H([i2],[i2]);
H3=H([i3],[i3]);H4=H([i4],[i4]);
%Finding eigenvalues and eigenvectors
% for Hamiltonians with 0,1,2,3and 4 electrons
[V0,D0]=eig(H0);D0=real(sum(D0))';[a0,b0]=sort(D0);
[V1,D1]=eig(H1);D1=real(sum(D1))';[a1,b1]=sort(D1);
[V2,D2]=eig(H2);D2=real(sum(D2))';[a2,b2]=sort(D2);
[V3,D3]=eig(H3);D3=real(sum(D3))';[a3,b3]=sort(D3);
[V4,D4]=eig(H4);D4=real(sum(D4))';[a4,b4]=sort(D4);
%need annihilation operator between ’g’ and ’h’ subspaces
 c11=c1([i0],[i1]);c12=c2([i0],[i1]);
 c13=c3([i0],[i1]);c14=c4([i0],[i1]);
 c21=c1([i1],[i2]);c22=c2([i1],[i2]);
 c23=c3([i1],[i2]);c24=c4([i1],[i2]);
 c31=c1([i2],[i3]);c32=c2([i2],[i3]);
 c33=c3([i2],[i3]);c34=c4([i2],[i3]);
 c41=c1([i3],[i4]);c42=c2([i3],[i4]);
 c43=c3([i3],[i4]);c44=c4([i3],[i4]);
%Evaluate gamma1 and gamma2 using Fermi’s Golden Rule
s0=size([i0],1);s1=size([i1],1);s2=size([i2],1);
s3=size([i3],1);s4=size([i4],1);
for ct1=1:s0
for ct2=1:s1
    eh1(ct1,ct2)=a1(ct2)-a0(ct1);
    ev0(:,ct1)=V0(:,b0(ct1));
    ev1(:,ct2)=V1(:,b1(ct2));
end
end
A11=[c11*ev1]'*ev0;
B11=[c12*ev1]'*ev0;
gam11=gamma1*((A11.*A11)+(B11.*B11));
A21=[c13*ev1]'*ev0;
B21=[c14*ev1]'*ev0;
gam21=gamma2*((A21.*A21)+(B21.*B21));
for ct1=1:s1
    for ct2=1:s2
        eh2(ct1,ct2)=a2(ct2)-a1(ct1);
        ev2(:,ct2)=V2(:,b2(ct2));
    end
end
A12=[c21*ev2]'*ev1;
B12=[c22*ev2]'*ev1;
gam12=gamma1*((A12.*A12)+(B12.*B12));
A22=[c23*ev2]'*ev1;
B22=[c24*ev2]'*ev1;
gam22=gamma2*((A22.*A22)+(B22.*B22));
ev3=zeros(s3,s3);
for ct1=1:s2
    for ct2=1:s3
        eh3(ct1,ct2)=a3(ct2)-a2(ct1);
        ev3(:,ct2)=V3(:,b3(ct2));
    end
end
A13=[c31*ev3]'*ev2;
B13=[c32*ev3]'*ev2;
gam13=gamma1*((A13.*A13)+(B13.*B13));
A23=[c33*ev3]'*ev2;
B23=[c34*ev3]'*ev2;
gam23=gamma2*((A23.*A23)+(B23.*B23));
for ct1=1:s3
    for ct2=1:s4
        eh4(ct1,ct2)=a4(ct2)-a3(ct1);
        ev4(:,ct2)=V4(:,b3(ct2));
    end
end
A14=[c41*ev4]'*ev3;
B14=[c42*ev4]'*ev3;
gam14=gamma1*((A14.*A14)+(B14.*B14));
A24=[c43*ev4]'*ev3;
B24=[c44*ev4]'*ev3;
gam24=gamma2*((A24.*A24)+(B24.*B24));
%muf=eh2(1,1)+0.000001;


    muL=muf+Vol;muR=muf;
      RL=zeros(16);
      R=zeros(16);
      f11=1./(1+exp((eh1-muL)/kBT));
      f21=1./(1+exp((eh1-muR)/kBT));
      f12=1./(1+exp((eh2-muL)/kBT));
      f22=1./(1+exp((eh2-muR)/kBT));
      f13=1./(1+exp((eh3-muL)/kBT));
      f23=1./(1+exp((eh3-muR)/kBT));
      f14=1./(1+exp((eh4-muL)/kBT));
      f24=1./(1+exp((eh4-muR)/kBT)); 
      sti=1;stj=2;eni=s0;enj=1+s1;
      R(sti:eni,stj:enj)=gam11'.*(1-f11)+gam21'.*(1-f21);
      RL(sti:eni,stj:enj)=-gam11'.*(1-f11);
      R(stj:enj,sti:eni)=(gam11'.*f11+gam21'.*f21)';
      RL(stj:enj,sti:eni)=(gam11'.*f11)';
      sti=eni+1;stj=enj+1;eni=sti+s1-1;enj=stj+s2-1;
      R(sti:eni,stj:enj)=gam12'.*(1-f12)+gam22'.*(1-f22);
      RL(sti:eni,stj:enj)=-gam12'.*(1-f12);
      R(stj:enj,sti:eni)=(gam12'.*f12+gam22'.*f22)';
      RL(stj:enj,sti:eni)=(gam12'.*f12)';
      sti=eni+1;stj=enj+1;eni=sti+s2-1;enj=stj+s3-1;
      R(sti:eni,stj:enj)=gam13'.*(1-f13)+gam23'.*(1-f23);
      RL(sti:eni,stj:enj)=-gam13'.*(1-f13);
      R(stj:enj,sti:eni)=(gam13'.*f13+gam23'.*f23)';
      RL(stj:enj,sti:eni)=(gam13'.*f13)';
      sti=eni+1;stj=enj+1;eni=sti+s3-1;enj=stj+s4-1;
      R(sti:eni,stj:enj)=gam14'.*(1-f14)+gam24'.*(1-f24);
      RL(sti:eni,stj:enj)=-gam14'.*(1-f14);
      R(stj:enj,sti:eni)=(gam14'.*f14+gam24'.*f24)';
      RL(stj:enj,sti:eni)=(gam14'.*f14)';
      for de=1:15
           R(de,de)=-sum(R(:,de));
      end
      R(16,:)=1;
      Pr=R\vec;
      I(g1,g2)=sum(RL*Pr);  
end
end
I=I0*I;
%Plots Coulomb Diamonds if gate voltage range is specified
figure(108)
[x,y] = meshgrid(Vg1,Vg2);
pcolor(x,y,I); 
colorbar;
shading interp
box on 