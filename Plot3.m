clear all 
np=100;
B1=linspace(0,-20,np);
B2=linspace(0,20,np);

d1_1=readmatrix('d1_1.txt');
d2_1=readmatrix('d2_1.txt');
d3_1=readmatrix('d3_1.txt');
d4_1=readmatrix('d4_1.txt');
d1_2=readmatrix('d1_2.txt');
d2_2=readmatrix('d2_2.txt');
d3_2=readmatrix('d3_2.txt');
d4_2=readmatrix('d4_2.txt');

figure(1)
    plot(B1,d1_1,'LineWidth',1)
    hold on 
    plot(B2,d1_2,'LineWidth',1)
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    %ylim([-0.015 0.015])
    %set ( gca, 'xdir', 'reverse' )
    
figure(2)
    plot(B1,d2_1,'LineWidth',1)
    hold on 
    plot(B2,d2_2,'LineWidth',1)
    ylim([-30e-3 30e-3])
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 

figure(3)
    plot(B1,d3_1,'LineWidth',1)
    hold on 
    plot(B2,d3_2,'LineWidth',1)
    ylim([-35e-3 35e-3])
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 

figure(4)
    plot(B1,d4_1,'LineWidth',1)
    hold on 
    plot(B2,d4_2,'LineWidth',1)
    ylim([-40e-3 40e-3])
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
