clear all
Vmax=-30e-3;Vmin=30e-3;
res=100;
Vol=1e-4;
%np=res;ng=1;  
%Vol=linspace(Vmin,Vmax,np);
hbar=1.055e-34;q=1.609e-19;I0=q^2/hbar;eta=0.5;
%Gate Voltage Range
muf=linspace(-10e-3,10e-3,res);nf=size(muf,2);
%muf=0.00;nf=1; 
gamma1=5e-6;
gamma2=5e-6;
vec=[zeros(1,15) 1]';kBT=0.000106;beta=0;
Vg=0;
%delv=[0.1 0.09];

c1=zeros(16);c2=zeros(16);c3=zeros(16);c4=zeros(16);
es=0e-3;del=0e-3;zp=0e-8; 
ep1=es;
U11=0e-3;
delta=5e-3; 

mu_B = 5.79e-5;
g_s = 2;
g_v = 15; 
%np=res;B_ll=linspace(0,20,np);B_per=0;B=B_ll;
np=res;B_per=linspace(0,20,np);B_ll=0;B=B_per;
hs = 0.5*g_s*mu_B*B_ll;
hv = 0.5*g_v*mu_B*B_per;

d0=[];
d1=[];
d2=[];
d3=[];
d4=[];

%annihilation operators c1,c2,c3,c4
for k1=1:2
for k2=1:2
for k3=1:2
         kp=[1 k3-1 k2-1 k1-1]*[8;4;2;1];  
         k=[0 k3-1 k2-1 k1-1]*[8;4;2;1];
         c4(k+1,kp+1)=(-1)^(k1-1+k2-1+k3-1);  
         kp=[k3-1 1 k2-1 k1-1]*[8;4;2;1];
         k=[k3-1 0 k2-1 k1-1]*[8;4;2;1];
         c3(k+1,kp+1)=(-1)^(k1-1+k2-1);  
         kp=[k3-1 k2-1 1 k1-1]*[8;4;2;1];
         k=[k3-1 k2-1 0 k1-1]*[8;4;2;1];
         c2(k+1,kp+1)=(-1)^(k1-1);
         kp=[k3-1 k2-1 k1-1 1]*[8;4;2;1];
         k=[k3-1 k2-1 k1-1 0]*[8;4;2;1];
         c1(k+1,kp+1)=1;            
end
end
end

for vc=1:np 
%i0,i1,i2,i3,i4 contain a list of columnends corresponding to
%states with 0,1,2,3 and 4 electrons respectively
i4=[];i3=[];i2=[];i1=[];i0=[];
%diagonal terms of Hamiltonian matrix, H
% and number operator, N 
H=zeros(16);N=zeros(16); 
for k4=1:2
for k3=1:2
for k2=1:2
for k1=1:2
        k = [k4-1 k3-1 k2-1 k1-1]*[8;4;2;1];
        n1=k1-1+k2-1+k3-1+k4-1;
        n=n1;
        H(k+1,k+1)=ep1*n1 + (U11/2)*(n1^2-n1); %+...
            %(delta/2)*[k4-1 k3-1 k2-1 k1-1]*[1;1;-1;-1] +...
            %hs(vc)*[k4-1 k3-1 k2-1 k1-1]*[1;-1;-1;1] +... 
            %hv(vc)*[k4-1 k3-1 k2-1 k1-1]*[1;-1;1;-1]; 
        N(k+1,k+1)=n; 
        if n==0
                i0=[k+1;i0];
        end
        if n==1
                i1=[k+1;i1];
        end
        if n==2
                i2=[k+1;i2];
        end
        if n==3
                i3=[k+1;i3];
        end
        if n==4
                i4=[k+1;i4];
        end
end
end
end
end

H = H + (delta/2)*(-c1'*c1-c2'*c2+c3'*c3+c4'*c4);
H = H + hs*(c1'*c1-c2'*c2-c3'*c3+c4'*c4);
H = H + hv(vc)*(-c1'*c1+c2'*c2-c3'*c3+c4'*c4);

%Separating the parts of the Hamiltonian
%with different numbers of electrons
H0=H([i0],[i0]);H1=H([i1],[i1]);H2=H([i2],[i2]);
H3=H([i3],[i3]);H4=H([i4],[i4]);
%Finding eigenvalues and eigenvectors
% for Hamiltonians with 0,1,2,3 and 4 electrons
[V0,D0]=eig(H0);D0=real(sum(D0))';[a0,b0]=sort(D0);
[V1,D1]=eig(H1);D1=real(sum(D1))';[a1,b1]=sort(D1); 
[V2,D2]=eig(H2);D2=real(sum(D2))';[a2,b2]=sort(D2);
[V3,D3]=eig(H3);D3=real(sum(D3))';[a3,b3]=sort(D3);
[V4,D4]=eig(H4);D4=real(sum(D4))';[a4,b4]=sort(D4);

d0(vc,:)=D0;
d1(vc,:)=D1;
d2(vc,:)=D2;
d3(vc,:)=D3;
d4(vc,:)=D4;

%figure(72)
    %plot(muf*(1),I(vc,:)*(1),'LineWidth',5);  
    %set(gca,'FontSize',48)
    %xlabel('V_D (mV)','FontSize',48)
    %ylabel('I_D (pA)','FontSize',48) 
end

%Plots Coulomb Diamonds if gate voltage range is specified
%figure(1)
%    plot(B,d0,'LineWidth',1)
figure(1)
    plot(B,d1,'LineWidth',1)
    xlabel('B_{\perp} (T)','FontSize',15)
    ylabel('E (eV)','FontSize',15) 
    ylim([-0.015 0.015])
    %set ( gca, 'xdir', 'reverse' )
figure(2)
    plot(B,d2,'LineWidth',1)
    ylim([-20e-3 20e-3])
    xlabel('B_{\perp} (T)','FontSize',15)
    ylabel('E (eV)','FontSize',15) 
    %set ( gca, 'xdir', 'reverse' )
%figure(4)
%    plot(B,d3,'LineWidth',1)
%figure(5)
%    plot(B,d4,'LineWidth',1)

    %heatmap(I,muf,Vol);
    