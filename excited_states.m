%rate equations
%mu_N_ij
%N number of e-
%ij the different states
clear all 
resolx = 300; % resolution in x-direction
resoly = 300; % resolution in y-direction
e = 1.602*10^-19; %elementary [C]
C_sum = 1*10^(-17); %capacitance in island [F]
g = 2.002319; %unitless g-factor for zeeman
mu_B = 9.27400968 * 10^(-24); %bohr magnetron [J/T]
B = 20; %magnetic field [T]
alpha_g=1/20; 
delta_E_b = B * mu_B * g; %splitting of levels [J]
E_c = e^2 / C_sum; %charging energy [J]
V = linspace(-25*10^(-3),25*10^(-3),resoly); %source-drain-bias [V]
Vg=linspace(-0.6,0.3,resolx); %gate voltage [V]
I_L=zeros(numel(V),numel(Vg));
gamma=10^(9);

for k=1:numel(V)
    for i=1:numel(Vg)
        mu_L = e*V(k)/2; % chemical potential in lead left [J]
        mu_R = -e*V(k)/2; % chemical potential in lead right [J]
        % ---------Chemical potentials in island-----------------------
        mu_1_11 = Vg(i)*e*alpha_g;
        mu_1_21 = Vg(i)*e*alpha_g+delta_E_b;
        mu_2_12 = Vg(i)*e*alpha_g+E_c;
        mu_2_11 = Vg(i)*e*alpha_g+E_c + delta_E_b ;
        % ---------Rate of tunneling-----------------------------------
        % gamma_k_r = 10^9 in this simplified case
        T = ; %temperature [K]
        %tunnel in
        W_11_01 = [f(mu_1_11,mu_L,T) f(mu_1_11,mu_R,T)];
        W_12_01 = [f(mu_1_21,mu_L,T) f(mu_1_21,mu_R,T)] ;
        W_21_12 = [f(mu_2_12,mu_L,T) f(mu_2_12,mu_R,T)] ;
        W_21_11 = [f(mu_2_11,mu_L,T) f(mu_2_11,mu_R,T)] ;
        %tunnel out
        W_01_11 = [(1-f(mu_1_11,mu_L,T)) (1-f(mu_1_11,mu_R,T))];
        W_01_12 = [(1-f(mu_1_21,mu_L,T)) (1-f(mu_1_21,mu_R,T))];
        W_11_21 = [(1-f(mu_2_11,mu_L,T)) (1-f(mu_2_11,mu_R,T))];
        W_12_21 = [(1-f(mu_2_12,mu_L,T)) (1-f(mu_2_12,mu_R,T))];

        % ---------Final matrix---------------------------------------
        WMAT = [1 1 1 1;
                sum(W_11_01) -sum(W_01_11)-sum(W_21_11) 0 sum(W_11_21);
                sum(W_12_01) 0 -sum(W_01_12)-sum(W_21_12) sum(W_12_21);
                0 sum(W_21_11) sum(W_21_12) -sum(W_11_21)-sum(W_12_21)];
        % ---------Calculation of probabilities and current-----------
        R=[1;0;0;0];
        P = WMAT\R ;% P(1) 01; P(2) 11; P(3) 12; P(4) 21
        I_L(k,i) = -e*gamma*(-W_01_11(1)*P(2)-W_01_12(1)*P(3)+W_11_01(1)*P(1)-W_11_21(1)*P(4)+W_12_01(1)*P(1)-W_12_21(1)*P(4)+W_21_11(1)*P(2)+W_21_12(1)*P(3));
        %I_R = -e*(-W_01_11(2)*P(2)-W_01_12(2)*P(3)+W_11_01(2)*P(1)-W_11_21(2)*P(4)+W_12_01(2)*P(1)-W_12_21(2)*P(4)+W_21_11(2)*P(2)+W_21_12(2)*P(3))
    end 
end

dIsd=[];
dV = V(2)-V(1);
% ---------Calculation of conductance----------------------------------
for n=1:numel(Vg)
    dIsd=[dIsd, diff(I_L(:,n))./dV];
end

%{
figure(1)
surf(Vg,V,I_L,'EdgeColor','none','LineStyle','none','FaceLighting','phong')
view(2)
xlabel('Gate Bias V_G')
ylabel('source drain bias V_{sd}')
title('Diamond structure for singe electron transistor InP/InAs')
%} 
%{
figure(2)
plot(V,I_L(:,round(numel(V)/2)))
xlabel('Bias V_{sd}')
ylabel('current I')
title('Conductivity: sweeping V_{sd} over diamond mid-section')
%}

figure(3)
surf(Vg,V(1:end-1),dIsd,'EdgeColor','none','LineStyle','none','FaceLighting','phong')
view(2)
xlabel('Gate Bias V_G')
ylabel('source drain bias V_{sd}')
title('Diamond structure for singe electron transistor InP/InAs')



function fermi=f(e,u,T)
    fermi = 1/(1+exp((e-u)/(1.38e-23*T)));
end 

