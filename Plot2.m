clear all 
res=500;
np=2*res;
B1=linspace(0,-20,np);
B2=linspace(-20,20,np);

muf=linspace(-15e-3,15e-3,res);
muf1=linspace(-15e-3,15e-3,res);
muf2=linspace(-20e-3,20e-3,res);

d1_1=readmatrix('d1_1.txt');
d2_1=readmatrix('d2_1.txt');
d1_2=readmatrix('d1_2.txt');
d2_2=readmatrix('d2_2.txt');
d3_1=[flip(d1_1,1);d1_2];
d3_2=[flip(d2_1,1);d2_2];
figure(2)
    [y,x] = meshgrid(muf2,B2);
    pcolor(x,y,d3_2); 
    shading flat
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    colormap(slanCM('bilbao'))
figure(1)
    [y,x] = meshgrid(muf1,B2);
    pcolor(x,y,d3_1); 
    shading flat
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    colormap(slanCM('bilbao'))

    
    
  
