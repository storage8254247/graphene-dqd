clear all
%define constants
eps1=20;eps2=20;t=10;
U = 20;

%define N and H matrices
N = diag([ones(1,1)*0 ones(1,4)*1 ones(1,6)*2 ones(1,4)*3 ones(1,1)*4]);
H0 = 0;
h11 = [eps1 t;t eps2];H1 = blkdiag(h11,h11);
h21 = [2*eps1+U 0;0 2*eps2+U];
h22 = [eps1+eps2 0;0 eps1+eps2];
H2 = blkdiag(h21,h22,h22);H2(1:2,3:4)=t;H2(3:4,1:2)=t;
h31=[eps1+2*eps2+U t;t 2*eps1+eps2+U];H3 = blkdiag(h31,h31);
H4 = 2*eps1+2*eps2+2*U;
H = blkdiag(H0,H1,H2,H3,H4);

ii =1; for mu = 0:60
p = expm(-(H-mu*N));
rho = p/trace(p);
n(ii) = trace(rho*N);X(ii)=mu;ii=ii+1;
end

hold on;
grid on;
box on;
h=plot(X,n,'k');
set(h,'linewidth',2.0)
set(gca,'Fontsize',36)
xlabel('\mu / kT --->');
ylabel(' n ---> ');