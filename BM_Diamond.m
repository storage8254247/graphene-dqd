clear all
min=0;
% Physical constants
hbar=1.055e-34;q=1.609e-19;I0=q^2/hbar;eta=0.5;k_B=8.617e-5;
% electrochemical potential, temperature, polarization, and contact couplings
mu_0=0.00;nf=1;T_C=10;eta_C=0.0;gam=2*1e-5;
T_H=T_C/(1-eta_C);
kBTH=k_B*T_H;kBTC=k_B*T_C;s_p_H=1;s_p_C=0.2;asy=1;
g_H_u=0.5*(1+s_p_H)*gam;g_H_d=0.5*(1-s_p_H)*gam;
g_C_u=asy*0.5*(1+s_p_C)*gam;g_C_d=asy*0.5*(1-s_p_C)*gam;
% angles of contact polarization
theta_H=pi/2;theta_C=0;alpha=0;
phi_H=alpha;phi_C=alpha;
c_H=cos(theta_H/2)*exp(1i*phi_H/2);s_H=sin(theta_H/2)*exp(-1i*phi_H/2);
c_C=cos(theta_C/2)*exp(1i*phi_C/2);s_C=sin(theta_C/2)*exp(-1i*phi_C/2);
% Matrix elements
M_H_p01=c_H;M_H_p02=s_H;M_C_p01=c_C;M_C_p02=s_C;
M_H_n01=-conj(s_H);M_H_n02=conj(c_H);M_C_n01=-conj(s_C);M_C_n02=conj(c_C);
M_H_p13=-s_H;M_H_p23=c_H;M_C_p13=-s_C;M_C_p23=c_C;
M_H_n13=-conj(c_H);M_H_n23=-conj(s_H);M_C_n13=-conj(c_C);M_C_n23=-conj(s_C);
% Energy spectrum
e_d_1_0=0*k_B*T_H;e_d_2_0=0*k_B*T_H;
U=30*kBTH;
vec=[zeros(1,3) 1]';kBT=kBTC;beta=0;
%Voltage grid
Vmax=80*kBTC;Vmin=-80*kBTC;
V_G_max=10*kBTC;V_G_min=-40*kBTC;
nV=400;ng=400;
V_DS=linspace(Vmin,Vmax,nV);dV=V_DS(2)-V_DS(1);
vcond_G=0.5*(V_DS(2:nV)+V_DS(1:nV-1));
V_G=linspace(V_G_min,V_G_max,ng);
%Initializations
R_T=zeros(6,6);R_H=zeros(6,6);R_C=zeros(6,6);R_H_Q=zeros(6,6);I_H=zeros(4,4);I_H_S=zeros(1,nV);I_H_G=zeros(ng,nV);cond_G=zeros(ng,nV-1);
P_0 = zeros(1,400);
P_1 = P_0;P_up = P_0;P_dn=P_0;
P_d=P_0;
for ig=1:ng
    for iV=1:nV
        mu_H=mu_0-0.5*V_DS(iV);
        mu_C=mu_0+0.5*V_DS(iV);
        e_d_1=e_d_1_0+V_G(ig);e_d_2=e_d_2_0+V_G(ig);
        % Fermi dirac functions
        f_H_01=1/(1+exp((e_d_1-mu_H)/kBTH));f_H_02=1/(1+exp((e_d_2-mu_H)/kBTH));
        f_C_01=1/(1+exp((e_d_1-mu_C)/kBTC));f_C_02=1/(1+exp((e_d_2-mu_C)/kBTC));
        f_H_13=1/(1+exp((e_d_2+U-mu_H)/kBTH));f_H_23=1/(1+exp((e_d_1+U-mu_H)/kBTH));
        f_C_13=1/(1+exp((e_d_2+U-mu_C)/kBTC));f_C_23=1/(1+exp((e_d_1+U-mu_C)/kBTC));
        
        x_H_01=(e_d_1-mu_H)/kBTH;x_H_13=(e_d_2+U-mu_H)/kBTH;xr_H_01=(-e_d_1-mu_H)/kBTH;xr_H_13=(-e_d_2-U-mu_H)/kBTH;
        x_C_01=(e_d_1-mu_C)/kBTC;x_C_13=(e_d_2+U-mu_C)/kBTC;xr_C_01=(-e_d_1-mu_C)/kBTC;xr_C_13=(-e_d_2-U-mu_C)/kBTC;
        % Real and virtual transitions
        f_H_a01=f_H_01;f_H_r01=1-f_H_01;f_H_a13=f_H_13;f_H_r13=1-f_H_13;
        f_C_a01=f_C_01;f_C_r01=1-f_C_01;f_C_a13=f_C_13;f_C_r13=1-f_C_13;fac_a=1;fac_r=-1;
        P_H_a01=fac_a*real(fdigamma(0.5+(1i/(2*pi))*x_H_01));P_H_r01=fac_r*real(fdigamma(0.5+(1i/(2*pi))*x_H_01));
        P_C_a01=fac_a*real(fdigamma(0.5+(1i/(2*pi))*x_C_01));P_C_r01=fac_r*real(fdigamma(0.5+(1i/(2*pi))*x_C_01));
        P_H_a13=fac_a*real(fdigamma(0.5+(1i/(2*pi))*x_H_13));P_H_r13=fac_r*real(fdigamma(0.5+(1i/(2*pi))*x_H_13));
        P_C_a13=fac_a*real(fdigamma(0.5+(1i/(2*pi))*x_C_13));P_C_r13=fac_r*real(fdigamma(0.5+(1i/(2*pi))*x_C_13));
        % writing out individual equations for the rate matrices
        
        %ROW 1 ---- R11 through R16----Left (H) and right (C) contact components
        
        R_H(1,1)=-(g_H_u*(M_H_p01*conj(M_H_p01)+M_H_p02*conj(M_H_p02))*f_H_a01+g_H_d*(M_H_n01*conj(M_H_n01)+M_H_n02*conj(M_H_n02))*f_H_a01);
        R_C(1,1)=-(g_C_u*(M_C_p01*conj(M_C_p01)+M_C_p02*conj(M_C_p02))*f_C_a01+g_C_d*(M_C_n01*conj(M_C_n01)+M_C_n02*conj(M_C_n02))*f_C_a01);
        
        R_H(1,2)=g_H_u*M_H_p01*conj(M_H_p01)*f_H_r01+g_H_d*M_H_n01*conj(M_H_n01)*f_H_r01;
        R_C(1,2)=g_C_u*M_C_p01*conj(M_C_p01)*f_C_r01+g_C_d*M_C_n01*conj(M_C_n01)*f_C_r01;
        
        R_H(1,3)=g_H_u*M_H_p02*conj(M_H_p02)*f_H_r01+g_H_d*M_H_n02*conj(M_H_n02)*f_H_r01;
        R_C(1,3)=g_C_u*M_C_p02*conj(M_C_p02)*f_C_r01+g_C_d*M_C_n02*conj(M_C_n02)*f_C_r01;
        
        R_H(1,4)=0;
        R_C(1,4)=0;
        
        R_H(1,5)=g_H_u*M_H_p01*conj(M_H_p02)*f_H_r01+g_H_d*M_H_n01*conj(M_H_n02)*f_H_r01;
        R_C(1,5)=g_C_u*M_C_p01*conj(M_C_p02)*f_C_r01+g_C_d*M_C_n01*conj(M_C_n02)*f_C_r01;
        
        R_H(1,6)=g_H_u*M_H_p02*conj(M_H_p01)*f_H_r01+g_H_d*M_H_n02*conj(M_H_n01)*f_H_r01;
        R_C(1,6)=g_C_u*M_C_p02*conj(M_C_p01)*f_C_r01+g_C_d*M_C_n02*conj(M_C_n01)*f_C_r01;
        
        
        
        %ROW 2 ---- R21 through R26----Left (H) and right (C) contact components
        
        R_H(2,1)=g_H_u*(M_H_p01*conj(M_H_p01))*f_H_a01+g_H_d*(M_H_n01*conj(M_H_n01))*f_H_a01;
        R_C(2,1)=g_C_u*(M_C_p01*conj(M_C_p01))*f_C_a01+g_C_d*(M_C_n01*conj(M_C_n01))*f_C_a01;
        
        R_H(2,2)=-(g_H_u*(conj(M_H_p01)*M_H_p01)*f_H_r01+g_H_d*(conj(M_H_n01)*M_H_n01)*f_H_r01+g_H_u*(conj(M_H_p13)*M_H_p13)*f_H_a13+g_H_d*(conj(M_H_n13)*M_H_n13)*f_H_a13);
        R_C(2,2)=-(g_C_u*(conj(M_C_p01)*M_C_p01)*f_C_r01+g_C_d*(conj(M_C_n01)*M_C_n01)*f_C_r01+g_C_u*(conj(M_C_p13)*M_C_p13)*f_C_a13+g_C_d*(conj(M_C_n13)*M_C_n13)*f_C_a13);
        
        R_H(2,3)=0;
        R_C(2,3)=0;
        
        R_H(2,4)=g_H_u*(M_H_p13*conj(M_H_p13))*f_H_r13+g_H_d*(M_H_n13*conj(M_H_n13))*f_H_r13;
        R_C(2,4)=g_C_u*(M_C_p13*conj(M_C_p13))*f_C_r13+g_C_d*(M_C_n13*conj(M_C_n13))*f_C_r13;
        
        R_H(2,5)=-0.5*g_H_u*((conj(M_H_p02)*M_H_p01)*(f_H_r01+(11i/pi)*P_H_r01)+(conj(M_H_p13)*M_H_p23)*(f_H_a13-(11i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n02)*M_H_n01)*(f_H_r01+(11i/pi)*P_H_r01)+(conj(M_H_n13)*M_H_n23)*(f_H_a13-(11i/pi)*P_H_a13));
        R_C(2,5)=-0.5*g_C_u*((conj(M_C_p02)*M_C_p01)*(f_C_r01+(11i/pi)*P_C_r01)+(conj(M_C_p13)*M_C_p23)*(f_C_a13-(11i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n02)*M_C_n01)*(f_C_r01+(11i/pi)*P_C_r01)+(conj(M_C_n13)*M_C_n23)*(f_C_a13-(11i/pi)*P_C_a13));
        
        R_H(2,6)=-0.5*g_H_u*((conj(M_H_p01)*M_H_p02)*(f_H_r01-(11i/pi)*P_H_r01)+(conj(M_H_p23)*M_H_p13)*(f_H_a13+(11i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n01)*M_H_n02)*(f_H_r01-(11i/pi)*P_H_r01)+(conj(M_H_n23)*M_H_n13)*(f_H_a13+(11i/pi)*P_H_a13));
        R_C(2,6)=-0.5*g_C_u*((conj(M_C_p01)*M_C_p02)*(f_C_r01-(11i/pi)*P_C_r01)+(conj(M_C_p23)*M_C_p13)*(f_C_a13+(11i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n01)*M_C_n02)*(f_C_r01-(11i/pi)*P_C_r01)+(conj(M_C_n23)*M_C_n13)*(f_C_a13+(11i/pi)*P_C_a13));
        
        
        
        %ROW 3 ----- R31 throught R36 --- left (H) and right (C) contact
        %components
        
        R_H(3,1)=g_H_u*(M_H_p02*conj(M_H_p02))*f_H_a01+g_H_d*(M_H_n02*conj(M_H_n02))*f_H_a01;
        R_C(3,1)=g_C_u*(M_C_p02*conj(M_C_p02))*f_C_a01+g_C_d*(M_C_n02*conj(M_C_n02))*f_C_a01;
        
        R_H(3,2)=0;
        R_C(3,2)=0;
        
        R_H(3,3)=-(g_H_u*(conj(M_H_p02)*M_H_p02)*f_H_r01+g_H_d*(conj(M_H_n02)*M_H_n02)*f_H_r01+g_H_u*(conj(M_H_p23)*M_H_p23)*f_H_a13+g_H_d*(conj(M_H_n23)*M_H_n23)*f_H_a13);
        R_C(3,3)=-(g_C_u*(conj(M_C_p02)*M_C_p02)*f_C_r01+g_C_d*(conj(M_C_n02)*M_C_n02)*f_C_r01+g_C_u*(conj(M_C_p23)*M_C_p23)*f_C_a13+g_C_d*(conj(M_C_n23)*M_C_n23)*f_C_a13);
        
        R_H(3,4)=g_H_u*(M_H_p23*conj(M_H_p23))*f_H_r13+g_H_d*(M_H_n23*conj(M_H_n23))*f_H_r13;
        R_C(3,4)=g_C_u*(M_C_p23*conj(M_C_p23))*f_C_r13+g_C_d*(M_C_n23*conj(M_C_n23))*f_C_r13;
        
        R_H(3,5)=-0.5*g_H_u*((conj(M_H_p02)*M_H_p01)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_p13)*M_H_p23)*(f_H_a13+(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n02)*M_H_n01)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_n13)*M_H_n23)*(f_H_a13+(1i/pi)*P_H_a13));
        R_C(3,5)=-0.5*g_C_u*((conj(M_C_p02)*M_C_p01)*(f_C_r01-(1i/pi)*P_C_r01)+(conj(M_C_p13)*M_C_p23)*(f_C_a13+(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n02)*M_C_n01)*(f_C_r01-(1i/pi)*P_C_r01)+(conj(M_C_n13)*M_C_n23)*(f_C_a13+(1i/pi)*P_C_a13));
        
        R_H(3,6)=-0.5*g_H_u*((conj(M_H_p01)*M_H_p02)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_p23)*M_H_p13)*(f_H_a13-(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n01)*M_H_n02)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_n23)*M_H_n13)*(f_H_a13-(1i/pi)*P_H_a13));
        R_C(3,6)=-0.5*g_C_u*((conj(M_C_p01)*M_C_p02)*(f_C_r01+(1i/pi)*P_C_r01)+(conj(M_C_p23)*M_C_p13)*(f_C_a13-(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n01)*M_C_n02)*(f_C_r01+(1i/pi)*P_C_r01)+(conj(M_C_n23)*M_C_n13)*(f_C_a13-(1i/pi)*P_C_a13));
        
        %ROW 4 ----- R41 throught R46 --- left (H) and right (C) contact
        %components
        
        R_H(4,1)=0;
        R_C(4,1)=0;
        
        R_H(4,2)=g_H_u*M_H_p13*conj(M_H_p13)*f_H_a13+g_H_d*M_H_n13*conj(M_H_n13)*f_H_a13;
        R_C(4,2)=g_C_u*M_C_p01*conj(M_C_p13)*f_C_a13+g_C_d*M_C_n13*conj(M_C_n13)*f_C_a13;
        
        R_H(4,3)=g_H_u*M_H_p23*conj(M_H_p23)*f_H_a13+g_H_d*M_H_n23*conj(M_H_n23)*f_H_a13;
        R_C(4,3)=g_C_u*M_C_p23*conj(M_C_p23)*f_C_a13+g_C_d*M_C_n23*conj(M_C_n23)*f_C_a13;
        
        R_H(4,4)=-(g_H_u*(M_H_p13*conj(M_H_p13)+M_H_p23*conj(M_H_p23))*f_H_r13+g_H_d*(M_H_n13*conj(M_H_n13)+M_H_n23*conj(M_H_n23))*f_H_r13);
        R_C(4,4)=-(g_C_u*(M_C_p13*conj(M_C_p13)+M_C_p23*conj(M_C_p23))*f_C_r13+g_C_d*(M_C_n13*conj(M_C_n13)+M_C_n23*conj(M_C_n23))*f_C_r13);
        
        R_H(4,5)=g_H_u*M_H_p23*conj(M_H_p13)*f_H_a13+g_H_d*M_H_n23*conj(M_H_n13)*f_H_a13;
        R_C(4,5)=g_C_u*M_C_p23*conj(M_C_p13)*f_C_a13+g_C_d*M_C_n23*conj(M_C_n13)*f_C_a13;
        
        R_H(4,6)=g_H_u*M_H_p13*conj(M_H_p23)*f_H_a13+g_H_d*M_H_n13*conj(M_H_n23)*f_H_a13;
        R_C(4,6)=g_C_u*M_C_p13*conj(M_C_p23)*f_C_a13+g_C_d*M_C_n13*conj(M_C_n23)*f_C_a13;
        
        
        %ROW 5 ----- R51 throught R56 --- left (H) and right (C) contact
        %components
        
        R_H(5,1)=g_H_u*(M_H_p02*conj(M_H_p01))*f_H_a01+g_H_d*(M_H_n02*conj(M_H_n01))*f_H_a01;
        R_C(5,1)=g_C_u*(M_C_p02*conj(M_C_p01))*f_C_a01+g_C_d*(M_C_n02*conj(M_C_n01))*f_C_a01;
        
        R_H(5,2)=-0.5*g_H_u*((conj(M_H_p01)*M_H_p02)*(f_H_r01+(11i/pi)*P_H_r01)+(conj(M_H_p23)*M_H_p13)*(f_H_a13-(11i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n01)*M_H_n02)*(f_H_r01+(11i/pi)*P_H_r01)+(conj(M_H_n23)*M_H_n13)*(f_H_a13-(1i/pi)*P_H_a13));
        R_C(5,2)=-0.5*g_C_u*((conj(M_C_p01)*M_C_p02)*(f_C_r01+(1i/pi)*P_C_r01)+(conj(M_C_p23)*M_C_p13)*(f_C_a13-(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n01)*M_C_n02)*(f_C_r01+(1i/pi)*P_C_r01)+(conj(M_C_n23)*M_C_n13)*(f_C_a13-(1i/pi)*P_C_a13));
        
        R_H(5,3)=-0.5*g_H_u*((conj(M_H_p01)*M_H_p02)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_p23)*M_H_p13)*(f_H_a13+(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n01)*M_H_n02)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_n23)*M_H_n13)*(f_H_a13+(1i/pi)*P_H_a13));
        R_C(5,3)=-0.5*g_C_u*((conj(M_C_p01)*M_C_p02)*(f_C_r01-(1i/pi)*P_C_r01)+(conj(M_C_p23)*M_C_p13)*(f_C_a13+(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n01)*M_C_n02)*(f_C_r01-(1i/pi)*P_C_r01)+(conj(M_C_n23)*M_C_n13)*(f_C_a13+(1i/pi)*P_C_a13));
        
        R_H(5,4)=g_H_u*(M_H_p13*conj(M_H_p23))*f_H_r13+g_H_d*(M_H_n13*conj(M_H_n23))*f_H_r13;
        R_C(5,4)=g_C_u*(M_C_p13*conj(M_C_p23))*f_C_r13+g_C_d*(M_C_n13*conj(M_C_n23))*f_C_r13;
        
        R_H(5,5)=-0.5*g_H_u*((conj(M_H_p01)*M_H_p01)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_p13)*M_H_p13)*(f_H_a13-(1i/pi)*P_H_a13)) ...
            -0.5*g_H_u*((conj(M_H_p02)*M_H_p02)*(f_H_r01-(11i/pi)*P_H_r01)+(conj(M_H_p23)*M_H_p23)*(f_H_a13+(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n01)*M_H_n01)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_n13)*M_H_n13)*(f_H_a13-(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n02)*M_H_n02)*(f_H_r01-(11i/pi)*P_H_r01)+(conj(M_H_n23)*M_H_n23)*(f_H_a13+(11i/pi)*P_H_a13));
        
        R_C(5,5)=-0.5*g_C_u*((conj(M_C_p01)*M_C_p01)*(f_C_r01+(11i/pi)*P_C_r01)+(conj(M_C_p13)*M_C_p13)*(f_C_a13-(1i/pi)*P_C_a13)) ...
            -0.5*g_C_u*((conj(M_C_p02)*M_C_p02)*(f_C_r01-(11i/pi)*P_C_r01)+(conj(M_C_p23)*M_C_p23)*(f_C_a13+(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n01)*M_C_n01)*(f_C_r01+(11i/pi)*P_C_r01)+(conj(M_C_n13)*M_C_n13)*(f_C_a13-(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n02)*M_C_n02)*(f_C_r01-(11i/pi)*P_C_r01)+(conj(M_C_n23)*M_C_n23)*(f_C_a13+(11i/pi)*P_C_a13));
        
        R_H(5,6)=0;
        R_C(5,6)=0;
        
        %ROW 6 ----- R61 throught R66 --- left (H) and right (C) contact
        %components
        
        R_H(6,1)=g_H_u*(M_H_p01*conj(M_H_p02))*f_H_a01+g_H_d*(M_H_n01*conj(M_H_n02))*f_H_a01;
        R_C(6,1)=g_C_u*(M_C_p01*conj(M_C_p02))*f_C_a01+g_C_d*(M_C_n01*conj(M_C_n02))*f_C_a01;
        
        R_H(6,2)=-0.5*g_H_u*((conj(M_H_p02)*M_H_p01)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_p13)*M_H_p23)*(f_H_a13+(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n02)*M_H_n01)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_n13)*M_H_n23)*(f_H_a13+(1i/pi)*P_H_a13));
        R_C(6,2)=-0.5*g_C_u*((conj(M_C_p02)*M_C_p01)*(f_C_r01-(1i/pi)*P_C_r01)+(conj(M_C_p13)*M_C_p23)*(f_C_a13+(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n02)*M_C_n01)*(f_C_r01-(1i/pi)*P_C_r01)+(conj(M_C_n13)*M_C_n23)*(f_C_a13+(1i/pi)*P_C_a13));
        
        R_H(6,3)=-0.5*g_H_u*((conj(M_H_p02)*M_H_p01)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_p13)*M_H_p23)*(f_H_a13-(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n02)*M_H_n01)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_n13)*M_H_n23)*(f_H_a13-(1i/pi)*P_H_a13));
        R_C(6,3)=-0.5*g_C_u*((conj(M_C_p02)*M_C_p01)*(f_C_r01+(1i/pi)*P_C_r01)+(conj(M_C_p13)*M_C_p23)*(f_C_a13-(1i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n02)*M_C_n01)*(f_C_r01+(1i/pi)*P_C_r01)+(conj(M_C_n13)*M_C_n23)*(f_C_a13-(1i/pi)*P_C_a13));
        
        R_H(6,4)=g_H_u*(M_H_p23*conj(M_H_p13))*f_H_r13+g_H_d*(M_H_n23*conj(M_H_n13))*f_H_r13;
        R_C(6,4)=g_C_u*(M_C_p23*conj(M_C_p13))*f_C_r13+g_C_d*(M_C_n23*conj(M_C_n13))*f_C_r13;
        
        R_H(6,5)=0;
        R_C(6,5)=0;
        
        R_H(6,6)=-0.5*g_H_u*((conj(M_H_p02)*M_H_p02)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_p23)*M_H_p23)*(f_H_a13-(1i/pi)*P_H_a13)) ...
            -0.5*g_H_u*((conj(M_H_p01)*M_H_p01)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_p13)*M_H_p13)*(f_H_a13+(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n02)*M_H_n02)*(f_H_r01+(1i/pi)*P_H_r01)+(conj(M_H_n23)*M_H_n23)*(f_H_a13-(1i/pi)*P_H_a13)) ...
            -0.5*g_H_d*((conj(M_H_n01)*M_H_n01)*(f_H_r01-(1i/pi)*P_H_r01)+(conj(M_H_n13)*M_H_n13)*(f_H_a13+(1i/pi)*P_H_a13));
        
        R_C(6,6)=-0.5*g_C_u*((conj(M_C_p02)*M_C_p02)*(f_C_r01+(11i/pi)*P_C_r01)+(conj(M_C_p23)*M_C_p23)*(f_C_a13-(11i/pi)*P_C_a13)) ...
            -0.5*g_C_u*((conj(M_C_p01)*M_C_p01)*(f_C_r01-(11i/pi)*P_C_r01)+(conj(M_C_p13)*M_C_p13)*(f_C_a13+(11i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n02)*M_C_n02)*(f_C_r01+(11i/pi)*P_C_r01)+(conj(M_C_n23)*M_C_n23)*(f_C_a13-(11i/pi)*P_C_a13)) ...
            -0.5*g_C_d*((conj(M_C_n01)*M_C_n01)*(f_C_r01-(11i/pi)*P_C_r01)+(conj(M_C_n13)*M_C_n13)*(f_C_a13+(1i/pi)*P_C_a13));
        
        % ----------------------------------------------------------------------------------------------------------------------------------
        R_T=R_H+R_C;eta=1e-6;
        R_T(4,1:4)=1;R_T(4,5)=0;R_T(4,6)=0;
        vec=[0 0 0 1 0 0]';
        P_vec_nH=pinv(R_T)*vec;rho=[P_vec_nH(1) 0 0 0; 0 P_vec_nH(2) P_vec_nH(5) 0; 0 P_vec_nH(6) P_vec_nH(3) 0; 0 0 0 P_vec_nH(4)];rho_H=0.5*(rho+rho');
        P_vec(1)=rho_H(1,1);P_vec(2)=rho_H(2,2);P_vec(3)=rho_H(3,3);P_vec(4)=rho_H(4,4);P_vec(5)=rho_H(2,3);P_vec(6)=rho_H(3,2);
        P_0(iV)=P_vec(1);
        P_1(iV)=P_vec(2)+P_vec(3);P_up(iV)=P_vec(2);P_dn(iV)=P_vec(3);
        P_d(iV)=P_vec(4);
        %---------------------Current calculation-------------------------------
        I_H(1,1)=-g_H_u*(M_H_p01*conj(M_H_p01)*P_vec(2)+M_H_p02*conj(M_H_p02)*P_vec(3)+M_H_p01*conj(M_H_p02)*P_vec(5)+M_H_p02*conj(M_H_p01)*P_vec(6))*f_H_r01 ...
            -g_H_d*(M_H_n01*conj(M_H_n01)*P_vec(2)+M_H_n02*conj(M_H_n02)*P_vec(3)+M_H_n01*conj(M_H_n02)*P_vec(5)+M_H_n02*conj(M_H_n01)*P_vec(6))*f_H_r01;
        
        I_H(2,2)= g_H_u*(M_H_p01*conj(M_H_p01)*P_vec(1)*f_H_a01)+g_H_d*(M_H_n01*conj(M_H_n01)*P_vec(1)*f_H_a01) ...
            -g_H_u*(M_H_p13*conj(M_H_p13)*P_vec(4)*f_H_r13)-g_H_d*(M_H_n13*conj(M_H_n13)*P_vec(4)*f_H_r13);
        
        I_H(3,3)= g_H_u*(M_H_p02*conj(M_H_p02)*P_vec(1)*f_H_a01)+g_H_d*(M_H_n02*conj(M_H_n02)*P_vec(1)*f_H_a01) ...
            -g_H_u*(M_H_p23*conj(M_H_p23)*P_vec(4)*f_H_r13)-g_H_d*(M_H_n23*conj(M_H_n23)*P_vec(4)*f_H_r13);
        
        I_H(4,4)=g_H_u*(M_H_p13*conj(M_H_p13)*P_vec(2)+M_H_p23*conj(M_H_p23)*P_vec(3)+M_H_p23*conj(M_H_p13)*P_vec(5)+M_H_p13*conj(M_H_p23)*P_vec(6))*f_H_a13 ...
            +g_H_d*(M_H_n13*conj(M_H_n13)*P_vec(2)+M_H_n23*conj(M_H_n23)*P_vec(3)+M_H_n23*conj(M_H_n13)*P_vec(5)+M_H_n13*conj(M_H_n23)*P_vec(6))*f_H_a13;
        
        I_H_S(iV)=-I0*real(trace(I_H));
        I_H_G(ig,iV)=I_H_S(iV);
        
        %     % Left transition rate matrix for electric and heat current
        %      R_H=[0  -R_H_10 -R_H_20 0;R_H_01 0 0 -R_H_31;R_H_02 0 0 -R_H_32;0 R_H_13 R_H_23 0];
        %      R_H_Q=[0  -(e_d_1-mu_H)*R_H_10 -(e_d_2-mu_H)*R_H_20 0;(e_d_1-mu_H)*R_H_01 0 0 -(e_d_2+U-mu_H)*R_H_31;
        %          (e_d_2-mu_H)*R_H_02 0 0 -(e_d_1+U-mu_H)*R_H_32;0 (e_d_2+U-mu_H)*R_H_13 (e_d_1+U-mu_H)*R_H_23 0];
        %      R_inv=R_T;
        %      R_inv(4,:)=1;
        %      vec=[0 0 0 1]';
        %      P_vec=R_inv\vec;
        %      I(iV)=-I0*sum(R_H*P_vec);
        %      I_Q_H(iV)=(I0)*sum(R_H_Q*P_vec);
        %      P_d(iV)=I(iV)*V_DS(iV);
        %      eff(iV)=P_d(iV)/I_Q_H(iV);
    end
    cond_G(ig,1:nV-1)=(I_H_G(ig,2:nV)-I_H_G(ig,1:nV-1))/dV;
end
% figure(23)
% hold on
% set(gca,'FontSize',60);
% plot(V_DS/kBTC,I_H_S*(1e9),'LineWidth',8)
% xlabel('V_{app}/k_BT','Fontsize',72);
% ylabel('I (nA)','Fontsize',72);
%Plots Coulomb Diamonds if gate voltage range is specified
figure(129)
[x,y] = meshgrid(vcond_G/kBTC,V_G/kBTC);
pcolor(y,x,cond_G*(1e9));
colorbar;
shading interp
box on
set(gca,'FontSize',24);
ylabel('-qV_{app}/k_BT','Fontsize',24);
xlabel('(\epsilon-\mu_0)/k_BT','Fontsize',24);