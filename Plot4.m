clear all 
res=500;
np=2*res;
B1=linspace(0,-20,np);
B2=linspace(-20,20,np);

muf1=linspace(-15e-3,15e-3,res);
muf2=linspace(-30e-3,30e-3,res);
muf3=linspace(-35e-3,35e-3,res);
muf4=linspace(-40e-3,40e-3,res);

d1_1=readmatrix('d1_1.txt');
d2_1=readmatrix('d2_1.txt');
d3_1=readmatrix('d3_1.txt');
d4_1=readmatrix('d4_1.txt');
d1_2=readmatrix('d1_2.txt');
d2_2=readmatrix('d2_2.txt');
d3_2=readmatrix('d3_2.txt');
d4_2=readmatrix('d4_2.txt');
d5_1=[flip(d1_1,1);d1_2];
d5_2=[flip(d2_1,1);d2_2];
d5_3=[flip(d3_1,1);d3_2];
d5_4=[flip(d4_1,1);d4_2];

figure(4)
    [y,x] = meshgrid(muf4,B2);
    pcolor(x,y,d5_4); 
    shading flat
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    colormap(slanCM('bilbao'))

figure(3)
    [y,x] = meshgrid(muf3,B2);
    pcolor(x,y,d5_3); 
    shading flat
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    colormap(slanCM('bilbao'))

figure(2)
    [y,x] = meshgrid(muf2,B2);
    pcolor(x,y,d5_2); 
    shading flat
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    colormap(slanCM('bilbao'))

figure(1)
    [y,x] = meshgrid(muf1,B2);
    pcolor(x,y,d5_1); 
    shading flat
    xlabel('B (T)','FontSize',15)
    ylabel('E','FontSize',15) 
    colormap(slanCM('bilbao'))
    
    
  
