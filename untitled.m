clear all
c1=zeros(4);c2=zeros(4);
for k1=1:2
         kp=[1 k1-1]*[2;1];
         k=[0 k1-1]*[2;1]; 
         c2(k+1,kp+1)=(-1)^(k1-1);
         kp=[k1-1 1]*[2;1];
         k=[k1-1 0]*[2;1];
         c1(k+1,kp+1)=1;                  
end
i0=[];i1=[];i2=[];
for k1=1:2
for k2=1:2
    k=[k2-1 k1-1]*[2;1];
    n = k1-1+k2-1;
    if n==0
            i0=[k+1;i0];
    end
    if n==1
            i1=[k+1;i1];
    end
    if n==2
            i2=[k+1;i2];
    end
end 
end 

c11=c1([i0],[i1]);c12=c2([i0],[i1]);
c21=c1([i1],[i2]);c22=c2([i1],[i2]);
