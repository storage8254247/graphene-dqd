clear all
res=500;

nf=res;

muf1=linspace(-15e-3,15e-3,res);
muf2=linspace(-30e-3,30e-3,res);
muf3=linspace(-35e-3,35e-3,res);
muf4=linspace(-40e-3,40e-3,res);

%{
muf=linspace(40e-3,60e-3,res);
muf1=linspace(39e-3,40e-3,res);
muf2=linspace(85e-3,115e-3,res);
muf3=linspace(130e-3,170e-3,res);
muf4=linspace(175e-3,225e-3,res);
muf5=linspace(240e-3,260e-3,res);
%}
np=res;B=linspace(0,20,np);

cond = readmatrix('cond.txt');

cond1=zeros(np,nf);
cond2=zeros(np,nf);
cond3=zeros(np,nf);
cond4=zeros(np,nf);
cond5=zeros(np,nf);

for i=1:np 
    [pks,locs] = findpeaks(cond(i,:),'MinPeakHeight',1e-8);
    cond1(i,locs)=1;
    u=muf1(locs);
    
    v2=nchoosek(u,2);
    w2=sum(v2,2);
    z2=interp1(muf2,muf2,w2,'nearest');
    for j=1:length(z2)
        locs2(j)=find(muf2==z2(j));
    end 
    cond2(i,locs2)=1;
    
    
    if length(locs)>=3
    v3=nchoosek(u,3);
    w3=sum(v3,2);
    z3=interp1(muf3,muf3,w3,'nearest');
    for j=1:length(z3)
        locs3(j)=find(muf3==z3(j));
    end 
    cond3(i,locs3)=1;
    end
    
    if length(locs)>=4
    v4=nchoosek(u,4);
    w4=sum(v4,2);
    z4=interp1(muf4,muf4,w4,'nearest');
    for j=1:length(z4)
        locs4(j)=find(muf4==z4(j));
    end 
    cond4(i,locs4)=1;
    end 
    %{
    if length(locs)>=5
    v5=nchoosek(u,5);
    w5=sum(v5,2);
    z5=interp1(muf5,muf5,w5,'nearest');
    for j=1:length(z5)
        locs5(j)=find(muf5==z5(j));
    end 
    cond5(i,locs5)=1;
    end 
    %}
    
end 

writematrix(cond1);
writematrix(cond2);
writematrix(cond3);
writematrix(cond4);

figure(4)
    [y,x] = meshgrid(muf4,B);
    pcolor(x,y,cond4); 
    shading flat
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15)
    colormap(slanCM('bilbao'))
figure(3)
    [y,x] = meshgrid(muf3,B);
    pcolor(x,y,cond3); 
    shading flat
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15)
    colormap(slanCM('bilbao'))
figure(2)
    [y,x] = meshgrid(muf2,B);
    pcolor(x,y,cond2); 
    shading flat
    xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15)
    colormap(slanCM('bilbao'))
figure(1)
    [y,x] = meshgrid(muf1,B);
    pcolor(x,y,cond1); 
    shading flat
   xlabel('B_{||} (T)','FontSize',15)
    ylabel('E','FontSize',15)
    colormap(slanCM('bilbao'))
